node default {
  package { 'toml-rb':
    ensure   => present,
    provider => 'puppetserver_gem'
  }

  resources { 'firewall':
    purge => true,
  }
  Firewall {
    before  => Class['firewall::post'],
    require => Class['firewall::pre'],
  }

  class { ['firewall::pre', 'firewall::post']: }
  class { 'firewall': }



  firewall { '200 Allow Puppet Master':
    dport  => '8140',
    proto  => 'tcp',
    action => 'accept',
  }
  exec { 'apt-update':
    command => '/usr/bin/apt-get update'
  }
  #Installation et configuration de apache / Virtual host
  class { 'apache':
    default_vhost => false,
    mpm_module    => 'prefork'
  }
 
  class { 'apache::mod::php': 
  
  }

  apache::vhost { 'kickass-dubinsky.dgr.ovh':
  servername      => 'kickass-dubinsky.dgr.ovh',
  port            => '80',
  docroot         => '/var/www',
  redirect_status => 'permanent',
  redirect_dest   => 'https://kickass-dubinsky.dgr.ovh'
}

apache::vhost {'kickass-dubinsky.dgr.ovh.ssl':
  servername    => 'kickass-dubinsky.dgr.ovh',
   port         => '443',
  ssl           => true,
  docroot       => '/var/www',
  suphp_engine        => 'off',
    ssl_chain           => '/etc/letsencrypt/live/kickass-dubinsky.dgr.ovh/chain.pem',
    ssl_key             => '/etc/letsencrypt/live/kickass-dubinsky.dgr.ovh/privkey.pem',
    ssl_cert            => '/etc/letsencrypt/live/kickass-dubinsky.dgr.ovh/cert.pem',
    proxy_preserve_host => 'on',
    proxy_requests      => true,
    proxy_pass          => {
      path => '/graphs',
      url  => 'http://localhost:8080'
    },
}
  # Instalation et configuration de mysql
  $override_options = {
    'mysqld' => {
      'bind-address' => '127.0.0.1'
    }
  }
  class { '::mysql::server':
    root_password           => 'password2ouf',
    remove_default_accounts => true,
    restart                 => true,
    override_options        => $override_options
  }
  mysql::db { 'grafana':
    user     => 'grafanaasi',
    password => 'grafanatest123!',
    host     => 'localhost',
    grant    => ['ALL'],
  }
  #installation de php
  class { '::php':
    ensure       => latest,
    manage_repos => true,
    fpm          => true,
    dev          => true,
    composer     => true,
    pear         => true,
    phpunit      => false,
  }
  file { '/var/www/test.php':
    content => template('template/index.php.erb')
  }

  #include telegraf

  class { 'grafana':
    cfg => {
      app_mode => 'production',
      server   => {
        root_url  => 'http://silly-darwin.my-ni.fr/graphs/',
        http_port => 8080,
      },
      database => {
        type     => 'mysql',
        host     => '127.0.0.1:3306',
        name     => 'grafana',
        user     => 'grafanaasi',
        password => 'grafanatest123!',
      },
      users    => {
        allow_sign_up => false,
      },
    },
  }
  class { 'telegraf':
    hostname => $facts['hostname'],
    outputs  => {
      'influxdb' => [
        {
          'urls'     => [ "http://influxdb0.${facts['domain']}:8086", "http://influxdb1.${facts['domain']}:8086" ],
          'database' => 'telegraf',
          'username' => 'telegraf',
          'password' => 'metricsmetricsmetrics123',
        }
      ]
    },
    inputs   => {
      'cpu' => [
        {
          'percpu'   => true,
          'totalcpu' => true,
        }
      ]
    }
  }

class {'influxdb::server':}
  class { 'letsencrypt':
    unsafe_registration => true,
  }

  letsencrypt::certonly { 'kickass-dubinsky.dgr.ovh':
    domains => ['kickass-dubinsky.dgr.ovh'],
    plugin  => 'apache',
  }
#  class { 'ghost':
#    include_nodejs => true,
#  } -> ghost::blog { 'my_blog': }
}
